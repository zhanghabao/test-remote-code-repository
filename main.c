#include <stdio.h>
#include<stdbool.h>

/* 1990年1月1日 是星期一 */
//判断闰年
bool is_leap(int year)
{
    return ((year % 4 == 0 && year % 100 != 0) || year % 400 == 0);
}
//返回天数
bool is_valid(int year ,int month, int day){
    if(year < 1990)
        return false;
    if(month<1||month>12)
    {
        return false;
    }
    return true;
    if(day<1||day>31)
    {
        return false;
    }
}
int get_monthdays(int month,bool leap)
{
    int days = 0;
    int monthdays[] = {0,31,28 + !!leap,31,30,31,30,31,31,30,31,30,31};
    for(int i = 0; i <month; i++)
    {
        days += monthdays[i];
    }
    return days;
}
int main()
{
    int year, month, day, week, leap;
    int sum = 0;
    int i;

    scanf("%d/%d/%d", &year, &month, &day);
    if (is_valid(year, month ,day) == false)
    {
        printf("wrong input");
        return 0;
    }
    

    for (i = 1990; i < year; i++)
    {
        if (is_leap(i))
        {
            sum += 366;
        }
        else
        {
            sum += 365;
        }
    }

    if (is_leap(year))
    {
        leap = 1;
    }
    else
    {
        leap = 0;
    }

    sum+= get_monthdays(month, leap);
    sum += day;
    week = sum % 7;

    char* weekday[] = {"日","一","二","三","四","五","六"};
    printf("%d年%d月%d日是星期%s\n", year, month, day,weekday[week]);
    return 0;
}